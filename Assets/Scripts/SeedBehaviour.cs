﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedBehaviour : MonoBehaviour
{

    // Components
    private GameObject sprite;
    private CircleCollider2D collider;

    // Start is called before the first frame update
    void Start()
    {
        sprite = transform.GetChild(0).gameObject;
        collider = GetComponent<CircleCollider2D>();
        sprite.SetActive(false);

        // For debugging maybe (automatically makes them active)
        sprite.SetActive(true);
        sprite.GetComponent<Pickuppable>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // private void OnTriggerEnter2D(Collider2D other) {
        // if (other.gameObject.name == "SightCollider")
        // {
        //     collider.enabled = false;
        //     sprite.SetActive(true);
        //     sprite.GetComponent<Pickuppable>().enabled = true;
        //     Debug.Log("Hit");
        // }

    // }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Flower")
        {
                Vector2 randomVector = new Vector2(Random.value * 8 - 4, Random.value * 8 - 4);
                Instantiate(this.gameObject, new Vector3(Mathf.Round(randomVector.x), Mathf.Round(randomVector.y), 0), Quaternion.identity);
                Destroy(this.transform.parent.gameObject);
        }
    }
}
