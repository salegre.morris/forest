﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundController : MonoBehaviour
{
    public GameObject tileSelected;
    private bool tileUsed;
    public GameObject flower;
    private PlayerController playerController;
    public bool canPlant;
    private bool isTileSelected;

    public static bool existsSelected;
    public static GroundController selected;
    // Start is called before the first frame update
    void Start()
    {
        canPlant = true;
        tileSelected = transform.GetChild(2).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            if(isTileSelected) {
                tileSelected.SetActive(true);
            } else {
                tileSelected.SetActive(false);
            }
        }
        catch (System.Exception)
        {
            throw;
        }
       
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.name == "HitPoint")
        {
            playerController = other.transform.parent.gameObject.GetComponent<PlayerController>();
            playerController.tileSelected = this;

            if (!existsSelected) {
                existsSelected = true;
            } else {
                selected.isTileSelected = false;
            }
            isTileSelected = true;
            selected = this;

        } else {
            // isTileSelected = false;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.name == "HitPoint" && isTileSelected){
            // existsSelected = false;
            isTileSelected = false;
            // playerController.tileSelected = null;
        } else {
            isTileSelected = false;
        }
    }
}
