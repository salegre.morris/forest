﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickuppable : MonoBehaviour
{
    public bool enabled;
    public PlayerStats playerStats;

    // Start is called before the first frame update
    void Start()
    {
        playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.name == "Player" && enabled)
        {
            playerStats.AddSeed();
            Animator anim = other.gameObject.GetComponent<Animator>();
            playerStats.seedPickupTimer = 0.4f;
            playerStats.ToggleBoardPosition((int)(this.transform.position.x + 3.5f), (int)(this.transform.position.y + 3.5f));
            anim.SetBool("gotSeed", true);
            Destroy(this.transform.parent.gameObject);
            // Destroy(gameObject);
            Debug.Log("is seed");
        }
    }
}
