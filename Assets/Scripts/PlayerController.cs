﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Basic Parameters
    public int speed = 64;

    //Animation Controllers
    public Animator anim;
    private bool isExtracting;
    public bool isPlanting; 
    private float actionCounter = 1f;
    public float extractCounter = 0f;
    public float plantCounter = 0f;
    private Vector3 hitPosition;

    // Components
    private Rigidbody2D playerRigidbody;
    private GameObject aoe;
    public PlayerStats playerStats;
    public GameObject hitPoint;
    public GroundController tileSelected;
    public GameObject flower;

    public Slider functionSlider;

    // Start is called before the first frame update
    void Start()
    {
        // Get Components
        playerRigidbody = GetComponent<Rigidbody2D>();   
        anim = GetComponent<Animator>();
        playerStats = GetComponent<PlayerStats>();
        functionSlider.transform.parent.gameObject.SetActive(false);
        // Get Children
        aoe = transform.GetChild(0).gameObject;

        // Other
        aoe.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerStats.IsGameover())
        {
        // Horizontal Component
            if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f && !isPlanting)
            {
                playerRigidbody.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime, playerRigidbody.velocity.y);
                hitPosition = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, 0f);
            }
            else
            {
                playerRigidbody.velocity = new Vector2(0f, playerRigidbody.velocity.y);
            }

            // Vertical Component
            if (Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0.5f && !isPlanting)
            {
                playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, Input.GetAxisRaw("Vertical") * speed * Time.deltaTime);
                hitPosition = new Vector3(0f, Input.GetAxisRaw("Vertical"), 0f);
            }
            else
            {
                playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, 0f);
            }

            hitPoint.transform.position = this.transform.position + hitPosition;

            //if (Input.GetButtonDown("Fire1") || isExtracting)
            //{
            //    if (extractCounter <= 0f)
            //    {
            //        // Do
            //        anim.SetBool("isExtracting", true);
            //        extractCounter = actionCounter;
            //        isExtracting = true;
            //        aoe.SetActive(true);
            //    }
            //    ExtractAbility();
            //    // } else if (playerStats.seedCount > 0 && (Input.GetButtonDown("Fire2") || isPlanting))
            //}
            //else 
            if (playerStats.seedCount > 0 && (Input.GetButtonDown("Fire1") || isPlanting))
            {
                if(tileSelected != null && !playerStats.CheckPosition((int)(tileSelected.transform.position.x + 3.5f), (int)(tileSelected.transform.position.y + 3.5f)))
                {
                    if (plantCounter <= 0f)
                    {
                        // Do
                        anim.SetBool("isPlanting", true);
                        plantCounter = actionCounter;
                        isPlanting = true;
                        functionSlider.transform.parent.gameObject.SetActive(true);
                        functionSlider.value = 0f;
                    }
                    Plant();
                }
            }

        }


    }

    //void ExtractAbility()
    //{
    //    extractCounter -= Time.deltaTime;

    //    if (extractCounter <= 0f) {
    //        // Stop Doing
    //        isExtracting = false;
    //        anim.SetBool("isExtracting", false);
    //        aoe.SetActive(false);
    //    }
    //}
    
    void Plant()
    {

            plantCounter -= Time.deltaTime;
            functionSlider.value += Time.deltaTime;

            Debug.Log("ol");

            if (plantCounter <= 0f) {
            // Do Once
                // if (playerStats.seedCount > 0 && tileSelected != null) {
                if (tileSelected != null) {
                    GameObject flowerInst = Instantiate(flower, tileSelected.transform.position, Quaternion.Euler(0, 0, 0));
                    tileSelected.tileSelected.SetActive(false);
                    playerStats.RemoveSeed();
                    playerStats.board[(int)(flowerInst.transform.position.x + 3.5f), (int)(flowerInst.transform.position.y + 3.5f)] = true;
                    functionSlider.value = 0f;
                    functionSlider.transform.parent.gameObject.SetActive(false);
                    tileSelected.canPlant = false;
                } else {
                    isPlanting = false;
                    anim.SetBool("isPlanting", false);
                }
                Debug.Log("Should Stop Planting");

                // Stop Doing
                isPlanting = false;
                anim.SetBool("isPlanting", false);
            }

    }

    //void Ability(string action, float counter, bool isDoing)
    //{

    //    counter -= Time.deltaTime;

    //    if (counter <= 0f) {
    //        isDoing = false;
    //        anim.SetBool(action, false);
    //    }

    //}

    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Seed") {
            Debug.Log(other.collider.gameObject.name);
        }
        else 
            Debug.Log(other.collider.gameObject.name);
    }

    public void SetAnimFalse()
    {
        anim.SetBool("gotSeed", false);
    }

 }
