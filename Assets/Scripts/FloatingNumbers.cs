﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingNumbers : MonoBehaviour
{

    public float verticalSpeed;
    public int value;
    public Text displayNumber;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (value == 0)
            displayNumber.text = "";
        else
            displayNumber.text = value.ToString();
            
        transform.position = new Vector3 (transform.position.x, 
                                          transform.position.y + verticalSpeed * Time.deltaTime, 
                                          transform.position.z);
    }
}