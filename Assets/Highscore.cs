﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highscore : MonoBehaviour
{
    private static bool highscoreExists;
    public int highscore = 0;

    private Text highscoreText;
    // Start is called before the first frame update
    void Start()
    {
        if (!highscoreExists) 
        {
            highscoreExists = true;
            DontDestroyOnLoad(transform.gameObject);
        } else {
            Destroy (gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

        highscoreText = GameObject.Find("HighscoreText").GetComponent<Text>();
        highscoreText.text = highscore + "";
        
    }

    public void UpdateHighscore(int newHighScore)
    {
        highscore = newHighScore;
    }
}
