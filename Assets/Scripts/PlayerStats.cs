﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public int seedCount;

    private int seedsRemaining;
    public int seedsInitial;
    public Text seedsRemainingText;

    public int playerScore;
    public Text playerScoreText;

    private float timeRemaining;
    public Text timeRemainingText;

    public float uiUpdateCounter;
    public bool loadLevel = true;

    public int level;
    public Text levelText;
    public GameObject seed;

    public float gameOverTimer = 5f;

    public float seedPickupTimer = 0f;

    public FloatingNumbers seedCounter;
    public GameObject seedNumbers;

    public bool[,] board;

    // Start is called before the first frame update
    void Start()
    {
        playerScore = 0;
        level = 1;
        timeRemaining = 4;
        board = new bool[8, 8];

        // ui
        playerScoreText = GameObject.Find("PlayerScore").GetComponent<Text>();
        timeRemainingText = GameObject.Find("TimeRemaining").GetComponent<Text>();
        levelText = GameObject.Find("LevelCounter").GetComponent<Text>();

        seedCounter = seedNumbers.GetComponent<FloatingNumbers>();

        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(loadLevel)
        {
            nextLevel();
        }

        if (timeRemaining > 0f) 
        {
            timeRemaining -= Time.deltaTime;    

            if (timeRemaining <= 3.0f)
            {
                Debug.Log("Changing Color");
                // timeRemainingText.color = new Color(171f, 30f, 30f, 250f);
                timeRemainingText.color = Color.red;
            } else {
                timeRemainingText.color = Color.white;
            }

            timeRemainingText.text = Mathf.Round((int)(timeRemaining*100)/100f)+ "";

            uiUpdateCounter -= Time.deltaTime;
            if (uiUpdateCounter <= 0f) 
            {
                playerScoreText.text = "Score: " + playerScore;
                seedsRemainingText.text = "Seeds Remaining: " + seedsRemaining;
                uiUpdateCounter = 0.5f;
            }

            if (seedsRemaining <= 0 && seedCount == 0) {
                // timeRemaining-= 1;
                // playerScore += seedsInitial;
                // if (timeRemaining <= 0) {
                //     loadLevel = true;
                //     // Application.LoadLevel("Level2");
                // }
                level++;
                timeRemaining += 3;
                loadLevel = true;
            }

            if(seedPickupTimer <= 0f)
            {
                this.gameObject.GetComponent<PlayerController>().SetAnimFalse();
            } else {
                seedPickupTimer -= Time.deltaTime;
            }

        } else {
            foreach (GameObject sd in GameObject.FindGameObjectsWithTag("Seed"))
            {
                Destroy(sd);
            }
            timeRemainingText.text = "Game Over";
            seedsRemainingText.text = "Score: " + playerScore;
            playerScoreText.text = "";
            
            gameOverTimer -= Time.deltaTime;

            if (gameOverTimer <= 0f) {
                GameObject.Find("Highscore").GetComponent<Highscore>().UpdateHighscore(playerScore);
                Destroy(GameObject.Find("Player"));
                Application.LoadLevel("Menu");
            }                
        }
        
    }

    public void AddSeed()
    {
        seedCount++;
        seedsRemaining--;
        timeRemaining += 2f;
        seedCounter.value = seedCount;
    }

    public void RemoveSeed()
    {
        seedCount--;
        timeRemaining += 0.3f;
        seedCounter.value = seedCount;
    }

    public void nextLevel()
    {
        levelText.text = "Level: " + level;
        // int seedsToLoad = (int)Mathf.Round(level * (1 + Mathf.Pow(1.1f, level)));
         int seedsToLoad = (int) Mathf.Round(level/2) + 1;

        if (seedsToLoad > EmptyBoardspaces())
        {
            timeRemainingText.text = "You Win!";
            timeRemaining = 0f;
        }
        else
        {
            for (int i = seedsToLoad; i > 0; i--)
            {
                Vector2 randomVector = new Vector2(Mathf.Round(Random.value * 7f), Mathf.Round(Random.value * 7f));
                Debug.Log("Vector X: " + (int)randomVector.x + " | Vector Y: " + (int)randomVector.y);
                if (board[(int)randomVector.x, (int)randomVector.y] == false)
                {
                    Instantiate(seed, new Vector3(randomVector.x - 3.5f, randomVector.y - 3.5f, 0f), Quaternion.identity);
                    ToggleBoardPosition((int)randomVector.x, (int)randomVector.y);
                }
                else
                {
                    i++;
                }

            }
        }


        seedsRemaining = GameObject.FindGameObjectsWithTag("Seed").Length;
        seedsInitial = seedsRemaining;
        seedsRemainingText = GameObject.Find("SeedsRemaining").GetComponent<Text>();

        // lock condition
        loadLevel = false;
    }

    public void ToggleBoardPosition(int x, int y)
    {
        board[x, y] = !board[x, y];
    }

    public bool CheckPosition(int x, int y)
    {
       return board[x, y];
    }

    public bool IsGameover()
    {
        if (gameOverTimer <= 0f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private int EmptyBoardspaces()
    {
        int counter = 0;
         for (int i = 0; i < board.GetLength(0); i++) 
         {
             for (int j = 0; j < board.GetLength(1); j++)
             {
                if (board[i, j] == false)
                    counter++;
             }
         }

        return counter;
    }

}
