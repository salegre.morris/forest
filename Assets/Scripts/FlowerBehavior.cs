﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerBehavior : MonoBehaviour
{
    public float age;
    public Animator animator;

    public GameObject player;
    private PlayerStats playerStats;

    public bool colided;

    private float addScoreTimer = 1f;
    // Start is called before the first frame update
    void Start()
    {
        age = 0;
        player = GameObject.Find("Player");
        playerStats = player.GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        age += Time.deltaTime;
        addScoreTimer -= Time.deltaTime;

        animator.SetFloat("age", age);

        if (addScoreTimer <= 0f) {
            playerStats.playerScore += 1;
            addScoreTimer = 1f;
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Seed")
            colided = true;
    }
}
